
Ubercart pending order notification MODULE FOR DRUPAL 7.x
-------------------------------------------------------

CONTENTS OF THIS README
-----------------------

   * Description
   * Installation
   * Configuration
   * Maintainers

DESCRIPTION:
------------

This module integrates with the Ubercart module and send notification email to admin and customer(optional)
when order is in pending after checkout.


INSTALLATION:
-------------
Install as you would normally install a contributed Drupal module.
See:
https://www.drupal.org/documentation/install/modules-themes/modules-7
for further information.


CONFIGURATION:
--------------

For configuration option you need to follow admin/config/pending-order-emails.



MAINTAINERS
--------------------------------------------------------------------
 Current maintainer:
 * Vikas kumar - https://www.drupal.org/u/babusaheb.vikas
